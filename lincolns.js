const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const lincolnsArticlesApiUrl = Deno.env.get("LINCOLNS_ARTICLES_API_URL");
const lincolnsItemsPerPage = Deno.env.get("LINCOLNS_ITEMS_PER_PAGE");
const lincolnsMaxArticles = Deno.env.get("LINCOLNS_MAX_ARTICLES");
const existingProductsAPI = Deno.env.get("PRODUCTS_API_URL");

let articleMap = {};

async function getExistingProducts() {
  try {
    let productsAPIResponse = await fetch(existingProductsAPI, {
      method: "GET",
    });
    if (productsAPIResponse) {
      console.log(`Fetched existing products in whitelist`);
      return productsAPIResponse.json();
    } else {
      return {};
    }
  } catch (e) {
    console.log(`Unable to fetch existing products: ${e.message}`);
    return {};
  }
}

function createLincolnsProductMap(lincolnsArticle) {
  let sku = lincolnsArticle.codigo;
  articleMap[sku] = {};

  for (let lincolnsVariant of lincolnsArticle.variantes) {
    let variantStock =
      lincolnsVariant.presentaciones[0].stock >= 5
        ? lincolnsVariant.presentaciones[0].stock - 5
        : 0;
    articleMap[sku][lincolnsVariant.presentaciones[0].sku] = {
      precio: lincolnsVariant.presentaciones[0].precioVenta["UYU"],
      stock: variantStock,
    };
  }
}

function createBlankArticle(articleSkuWithVariants) {
  let sku = articleSkuWithVariants.sku;
  let ecommercesToSend = Object.values(clientEcommerce).filter(
    (ecommerce_id) => ecommerce_id != "store"
  );

  let processedArticle = {
    sku: sku,
    client_id: clientId,
    options: {
      merge: false,
    },
    integration_id: clientIntegrationId,
    ecommerce: ecommercesToSend.map((ecommerce_id) => {
      return {
        ecommerce_id: ecommerce_id,
        properties: [],
        variants: articleSkuWithVariants.variants.map(createBlankVariant),
      };
    }),
  };

  return processedArticle;
}

function createBlankVariant(variantSku) {
  return {
    sku: variantSku,
    properties: [
      {
        stock: 0,
      },
    ],
  };
}

async function retrieveArticlesFromAPI(params) {
  try {
    let response = await fetch(params.url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params.data),
    });
    return response.json();
  } catch (e) {
    throw new Error(`Failed to retrieve articles: ${e.message}`);
  }
}

function processLincolnsArticle(articleSkuWithVariants) {
  if (articleMap[articleSkuWithVariants.sku]) {
    let sku = articleSkuWithVariants.sku;
    let ecommercesToSend = Object.values(clientEcommerce).filter(
      (ecommerce_id) => ecommerce_id != "store"
    );

    let articlePrice = Object.values(articleMap[sku])[0].precio;

    let processedArticle = {
      sku: sku,
      client_id: clientId,
      options: {
        merge: false,
      },
      integration_id: clientIntegrationId,
      ecommerce: ecommercesToSend.map((ecommerce_id) => {
        return {
          ecommerce_id: ecommerce_id,
          properties: [
            {
              price: {
                currency: "$",
                value: articlePrice,
              },
            },
          ],
          variants: articleSkuWithVariants.variants.map((variantSku) => {
            return processLincolnsVariant(sku, variantSku);
          }),
        };
      }),
    };

    return processedArticle;
  } else {
    return createBlankArticle(articleSkuWithVariants);
  }
}

function processLincolnsVariant(parentSku, variantSku) {
  if (articleMap[parentSku][variantSku]) {
    return {
      sku: variantSku,
      properties: [
        {
          stock: articleMap[parentSku][variantSku].stock,
        },
      ],
    };
  } else {
    return {
      sku: variantSku,
      properties: [
        {
          stock: 0,
        },
      ],
    };
  }
}

let maxArticlesToRetrieve = 0;
if (Number(lincolnsMaxArticles) !== -1) {
  maxArticlesToRetrieve = Math.min(
    Number(lincolnsMaxArticles),
    Number(lincolnsItemsPerPage)
  );
} else {
  maxArticlesToRetrieve = lincolnsItemsPerPage;
}
let requestParameters = {
  url: lincolnsArticlesApiUrl,
  data: {
    _idSolicitud: `sagal-sync-articles-${new Date()}`,
    desde: 0,
    total: maxArticlesToRetrieve,
  },
};

let retrievedData = await retrieveArticlesFromAPI(requestParameters);
retrievedData.data.productos.map(createLincolnsProductMap);
let existingProducts = await getExistingProducts();

for (let [parentSku, variantSkus] of Object.entries(existingProducts)) {
  let existingArticle = {
    sku: parentSku,
    variants: variantSkus,
  };

  try {
    let processedArticle = processLincolnsArticle(existingArticle);
    await sagalDispatch(processedArticle);
  } catch (e) {
    console.log(
      `Unable to process lincolns article ${parentSku}: ${e.message}`
    );
  }
}
