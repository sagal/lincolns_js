const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const lincolnsArticlesApiUrl = Deno.env.get("LINCOLNS_ARTICLES_API_URL");
const lincolnsItemsPerPage = Deno.env.get("LINCOLNS_ITEMS_PER_PAGE");
const lincolnsMaxArticles = Deno.env.get("LINCOLNS_MAX_ARTICLES");

let articleMap = {};

async function retrieveArticlesFromAPI(params) {
  try {
    let response = await fetch(params.url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params.data),
    });
    return response.json();
  } catch (e) {
    throw new Error(`Failed to retrieve articles: ${e.message}`);
  }
}

function processLincolnsArticle(article) {
  let sku = article.codigo;

  let processedArticle = {
    sku: sku,
    client_id: clientId,
    options: {
      merge: false,
      override_create: {
        send: false,
      },
      override_update: {
        send: false,
      },
    },
    integration_id: clientIntegrationId,
    ecommerce: [
      {
        ecommerce_id: "store",
        properties: [],
        variants: article.variantes.map(processLincolnsVariant),
      },
    ],
  };

  return processedArticle;
}

function processLincolnsVariant(variant) {
  let variantStock = 0;

  return {
    sku: variant.presentaciones[0].sku,
    properties: [
      {
        stock: variantStock,
      },
    ],
  };
}

let maxArticlesToRetrieve = 0;
if (Number(lincolnsMaxArticles) !== -1) {
  maxArticlesToRetrieve = Math.min(
    Number(lincolnsMaxArticles),
    Number(lincolnsItemsPerPage)
  );
} else {
  maxArticlesToRetrieve = lincolnsItemsPerPage;
}
let requestParameters = {
  url: lincolnsArticlesApiUrl,
  data: {
    _idSolicitud: `sagal-sync-articles-${new Date()}`,
    desde: 0,
    total: maxArticlesToRetrieve,
  },
};

let retrievedData = await retrieveArticlesFromAPI(requestParameters);

try {
  let unprocessedArticles = retrievedData.data.productos;
  let processedArticles = unprocessedArticles.map((article) =>
    processLincolnsArticle(article)
  );

  for (let article of processedArticles) {
    await sagalDispatch(article);
  }
} catch (e) {
  console.log(`Unable to perform Lincolns integration: ${e.message}`);
}
