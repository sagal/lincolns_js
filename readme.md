# Integration script for client: Lincolns

## Introduction

This script was created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. Lincolns is a client that mainly sends their data through a RESTful API service, exposing an endpoint that Sagal can request from. 

## Workflow

- Article data is retrieved from Lincolns' exposed API endpoint.
- The data is restructured and processed by article, being transformed to a Sagal-friendly format. 
- The restructured data is dispatched to Sagal. 


## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number. 
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number. 
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **LINCOLNS_ARTICLES_API_URL**: the address of the API endpoint where the data requests can be sent to. 
- **LINCOLNS_ITEMS_PER_PAGE**: the maximum number of items that should be retrieved in a single API request from Lincolns. 
- **LINCOLNS_MAX_ARTICLES**: the maximum number of articles that we want to retrieve. -1 means that there is no expected maximum. 

## Functions 

### retrieveArticlesFromAPI(Object params)

**Description:** fetches Lincolns' article data, according to the given parameters. Lincolns' endpoint requires a POST request with a JSON body that contains the following format:

```
    {
    "_idSolicitud":"sagal-sync-articles-<request date>",
    "desde":<request offset>,
    "total":<request limit>
    }
```

The response is returned using the json() function call. 

### processLincolnsArticle(Object article)

**Description:** restructures the article according to Sagal's standard format. The only properties that should be drawn from the data are the article's parent code, its pricing, the variants' codes and the variants' stock. 

The format of an article in Lincolns is as follows:

```
    {
    "codigo": "0030100", //The sku of the article
    "variantes": [
    {
    "codigo": "010029",
    "presentaciones": [
    {
    "codigo": "U",
    "nombre": "U",
    "sku": "0030100010029", //The variant's sku
    "stock": 1.00, //The variant's stock
    "precioLista": {
    "UYU": 3730.00000, 
    "USD": 86.7400000000000
    },
    "precioVenta": {
    "UYU": 3730.00000, //the article's pricing, this is the value that should be taken
    "USD": 86.7400000000000
    }
    }
    ]
    }
    ]
    }
```

### processLincolnsVariant(Object variant) 

**Description:** restructures the fetched variant's data through a map function call, rendering the data in a Sagal-friendly format.

## Constraints

### Stock

Whenever stock is assigned, 5 units must be substracted as a safety net for the article. If the article has 5 or less units in stock, then its total amount of stock should be set to 0. 